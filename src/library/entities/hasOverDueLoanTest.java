package library.entities;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class hasOverDueLoanTest {

	@Test
	void test() {
		Patron p=new Patron(null, null, null, 0, 0);
		int b=p.getNumberOfCurrentLoans();
		boolean a=p.hasOverDueLoans();
		assertEquals(false,a);
		assertEquals(0,b);
		
	}

}
