package library.entities;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class borrowFromLibraryTest {

	@Test
	void test() {
		Book b=new Book(null, null, null, 0);
		boolean a=b.isAvailable();
		boolean c=b.isOnLoan();
		assertEquals(true,a);
		assertEquals(false,c);
	}

}
