package library.entities;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class bookAvailableTest {

	@Test
	void test() {
		Book b=new Book(null, null, null, 0);
		int a=b.getId();
		String a1=b.getTitle();
		boolean d=b.isOnLoan();
		assertEquals(0,a);
		assertEquals(null,a1);
		assertEquals(false,d);
	}

}
